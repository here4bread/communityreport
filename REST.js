var mysql = require("mysql");
var request = require('request');
function REST_ROUTER(router,connection,md5) {
    var self = this;
    self.handleRoutes(router,connection,md5);
}

REST_ROUTER.prototype.handleRoutes= function(router,connection,md5) {

  //Get assets
  router.get("/assets",function(req,res){

    var latitude = req.query.lat;
    var longitude = req.query.lon;
    var radius = req.query.rad;

    var councilAssets = [];
    var exclusions = ["Bollard", "Hoop"]

    request('https://opencouncildata.cloudant.com/test1/_design/geo/_geo/public-toilets?lat=' + latitude + '&lon=' + longitude + '&radius=' + radius + '&include_docs=true', function (error, response, toilets_body) {
      request('https://opencouncildata.cloudant.com/test1/_design/geo/_geo/street-furniture?lat=' + latitude + '&lon=' + longitude + '&radius=' + radius + '&include_docs=true&limit=100&nearest=true', function (error, response, furniture_body) {
    
        //console.log('error:', error); // Print the error if one occurred
        //console.log('statusCode:', response && response.statusCode);
        //console.log('toilets_body:', toilets_body);
        
        toilets_body = JSON.parse(toilets_body); 
        furniture_body = JSON.parse(furniture_body); 
        
        for (var i = 0; i < toilets_body.rows.length; i++) {
          var asset = {
            "reference": "",
            "type": "",
            "name": "",
            "operator": "",
            "longitude": 0,
            "latitude": 0
          }; 
          asset.reference = (toilets_body.rows[i].doc.ref != null ? toilets_body.rows[i].doc.ref : null);
          asset.type = (toilets_body.rows[i].doc.properties.openCouncilDataTopic != null ? toilets_body.rows[i].doc.properties.openCouncilDataTopic : null);
          asset.name = (toilets_body.rows[i].doc.properties.name != null ? toilets_body.rows[i].doc.properties.name : null);
          asset.operator = (toilets_body.rows[i].doc.properties.operator != null ? toilets_body.rows[i].doc.properties.operator : null);
          //asset.longitude = (toilets_body.rows[i].doc.properties.lon != null ? toilets_body.rows[i].doc.properties.lon : null);
          //asset.latitude = (toilets_body.rows[i].doc.properties.lat != null ? toilets_body.rows[i].doc.properties.lat : null);
          //Use geometry/coordinates instead of doc/lat-long
          asset.longitude = (toilets_body.rows[i].geometry.type == "Point" ? toilets_body.rows[i].geometry.coordinates[0] : toilets_body.rows[i].geometry.coordinates[0][0]);
          asset.latitude = (toilets_body.rows[i].geometry.type == "Point" ? toilets_body.rows[i].geometry.coordinates[1] : toilets_body.rows[i].geometry.coordinates[0][1]);
          
          if (asset.name != null) {
            console.log("Asset name" + asset.name);
            console.log("Asset lonitude" + asset.longitude);
            councilAssets.push(asset);
          }
        }
        
        for (var i = 0; i < furniture_body.rows.length; i++) {
          if (exclusions.indexOf(furniture_body.rows[i].doc.properties.asset_type) != -1) {
            continue;
          }
          var asset = {
            "reference": "",
            "type": "",
            "name": "",
            "operator": "",
            "longitude": 0,
            "latitude": 0
          }; 
          asset.reference = (furniture_body.rows[i].doc.ref != null ? furniture_body.rows[i].doc.ref : null);
          asset.type = (furniture_body.rows[i].doc.type != "Feature" ? furniture_body.rows[i].doc.type : furniture_body.rows[i].doc.properties.asset_type);
          asset.name = (furniture_body.rows[i].doc.properties.description != null ? furniture_body.rows[i].doc.properties.description : null);
          asset.operator = (furniture_body.rows[i].doc.properties.company != null ? furniture_body.rows[i].doc.properties.company : null);
          asset.longitude = (furniture_body.rows[i].geometry.type == "Point" ? furniture_body.rows[i].geometry.coordinates[0] : furniture_body.rows[i].geometry.coordinates[0][0]);
          asset.latitude = (furniture_body.rows[i].geometry.type == "Point" ? furniture_body.rows[i].geometry.coordinates[1] : furniture_body.rows[i].geometry.coordinates[0][1]);
          
          if (asset.name != null) {
            console.log("Asset name" + asset.name);
            console.log("Asset lonitude" + asset.longitude);
            councilAssets.push(asset);
          }
        }

        res.json({"error" : false, "message" : "Success", "count" : councilAssets.length, "councilAssets" : councilAssets});
      });
    });
  });


  //Receive report
  router.post("/report",function(req,res){
    var query = "INSERT INTO ??(??,??,??,??,??,??,??,??,??) VALUES (?,?,?,?,?,?,?,?,?)";
    var table = ["reports","asset_ref","asset_type", "asset_operator", "asset_name", "asset_longitude", "asset_latitude", 
      "report_status", "report_description", "report_photo", req.body.ref,req.body.type, req.body.operator, 
      req.body.name, req.body.lon, req.body.lat, "Lodged", req.body.description, req.body.photo];
    query = mysql.format(query,table);
    connection.query(query,function(err,result){
      if(err) {
        res.json({"error" : true, "message" : "Error executing MySQL query", "errorMessage" : err});
      } else {
        res.json({"error" : false, "message" : "Report Added!", "report_id" : result.insertId});
      }
    });
  });


  //Action report??

}

module.exports = REST_ROUTER;
